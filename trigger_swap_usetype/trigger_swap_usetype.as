//trigger_swap_usetype: An entity which switches USE_TYPE ON to OFF and USE_TYPE OFF to ON!
// use #INCLUDE and add this to MapInit() of the main script: swapusetype::Register();
//Author: Puchi

namespace swapusetype
{
	class CTrigger_swap_usetype : ScriptBaseEntity
	{
		void Use( CBaseEntity@ pActivator, CBaseEntity@ pCaller, USE_TYPE useType, float flValue )
		{
			switch(useType)
			{
				case USE_ON:
					//g_Game.AlertMessage(at_console,"USeType ON: Switching to OFF\n");
					self.SUB_UseTargets( pCaller, USE_OFF, 0);
				break;

				case USE_OFF:
					//g_Game.AlertMessage(at_console,"USeType OFF: Switching to ON\n");
					self.SUB_UseTargets( pCaller, USE_ON, 0);
				break;

				default:
					//g_Game.AlertMessage(at_console,"USeType toggle or kill: No Change.\n");
					self.SUB_UseTargets( pCaller, useType, 0);
				break;
			}// end switch
		}// end void use

	} // End class


	void Register()
	{
		g_CustomEntityFuncs.RegisterCustomEntity( "swapusetype::CTrigger_swap_usetype", "trigger_swap_usetype" );
	}

}// end namespace

