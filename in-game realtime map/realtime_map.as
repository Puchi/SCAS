// Real-Time Player Display on miniature maps by Puchi
// This Map Plugin displays the current position of all players on a smaller area in real time inside the actual map.
//Big THANK YOU to Adambean, Duggles, JPolito, Nero, Sniper, Solokiller
//File goes to scripts/maps/realtime_map

bool devmsg = false; // enable this only when testing. Or else the server console will be spammed.
bool lotsofdevmsg = false; // even more spam!


//Mini Maps you have in your level.
//Format is: Coordinate X, Coordinate Y, Coordinate Z, Scale, Direction, Pin template name
//"-11","-39","-40","10","floor", "player_pin_", "player_pin_h_template"; means:
//this Map is at X -11, Y -39, Z -40; at scale 1:10 and on the floor.
//X, Y and Z coordinates are the origin 0/0/0 of the Mini Map (little red boxes in the example map).
//direction presets: floor, north, east, south, west. This will be used to convert the X/Y/Z coordinates of the players.
//Feel free to change it, but better leave it in for convenience and reference ;) You can easily add your own by just adding one to the ConvertAxis function
//Next, plaer_pin_" is the Pin name which references to the player. Number will be added automaticly and is a reference to the player index.
//"player_pin_h_template" is the Pin template you want to copy for this map from.

array<array<string>> MiniMaps =
{
	{"0","0","-316","10","floor", "player_pin_flr_", "player_pin_h_template"},
	{"0","892","16","10","north", "player_pin_n_", "player_pin_v_ns_template"},
	{"892","0","16","10","east", "player_pin_e_", "player_pin_v_ew_template"},
	{"0","-892","16","10","south", "player_pin_s_", "player_pin_v_ns_template"},
	{"-892","0","16","10","west", "player_pin_w_", "player_pin_v_ew_template"},
	{"0","0","-134","7","3D_Ceiling","player_pin_cus1_", "player_pin_h_template"}
};

float UpdateInterval = 1.0f; // how often the mini map(s) are updated. Don't set too low, might cause lag.

void MapInit()
{
	g_Hooks.RegisterHook(Hooks::Player::ClientPutInServer, @ClientPutInServer);
	g_Hooks.RegisterHook(Hooks::Player::ClientDisconnect, @ClientDisconnect);
	g_Scheduler.SetTimeout("UpdatePinPosition", UpdateInterval);
}

void UpdatePinPosition()
{
	for (int iPlayer = 1; iPlayer <= g_Engine.maxClients; ++iPlayer)
	{
	    CBasePlayer@ pPlayer = g_PlayerFuncs.FindPlayerByIndex(iPlayer);

	    if (pPlayer !is null && pPlayer.IsConnected())
	    {
			for (uint i = 0; i < MiniMaps.length; ++i)
			{
				Vector PinPosition = Vector(0,0,0);
				Vector MapPosition = Vector(atof(MiniMaps[i][0]),atof(MiniMaps[i][1]),atof(MiniMaps[i][2])); // Mini Map Position into Vector

				if (lotsofdevmsg)  g_Game.AlertMessage(at_console, "Mini Map %1: X: %2 - Y: %3 - Z: %4\n", i, MapPosition.x, MapPosition.y, MapPosition.z);
				if (lotsofdevmsg)  g_Game.AlertMessage(at_console, "Player %1 Origin: X: %2 - Y: %3 - Z: %4\n",iPlayer, pPlayer.pev.origin.x , pPlayer.pev.origin.y, pPlayer.pev.origin.z);

				PinPosition = pPlayer.pev.origin.opDiv(atof(MiniMaps[i][3])); // Fetch the player origin and scale it
				PinPosition = ConvertAxis(PinPosition,MapPosition,MiniMaps[i][4]); // Convert values to orientation

	        	if (lotsofdevmsg)  g_Game.AlertMessage(at_console, "Player %1 Origin calculated: X: %2 - Y: %3 - Z: %4\n",iPlayer, PinPosition.x , PinPosition.y, PinPosition.z);

	        	CBaseEntity@ PlayerPin = null;
				@PlayerPin = g_EntityFuncs.FindEntityByTargetname(null, MiniMaps[i][5] + iPlayer);

				if (PlayerPin !is null)
				{
					PlayerPin.SetOrigin(PinPosition);
					if (lotsofdevmsg) g_Game.AlertMessage(at_console, "Pin \"%1\" found, origin %2 set!\n- - - - - -\n", PlayerPin.GetTargetname(), PinPosition);
				}

			}// End for i < MiniMaps
		}//end player !is null
	}// end iplayer <= g_Engine.maxClients
	if (lotsofdevmsg) g_Game.AlertMessage(at_console, "--------------------\n");
	g_Scheduler.SetTimeout("UpdatePinPosition", UpdateInterval);

}

Vector ConvertAxis(Vector&in PlayerAxis, Vector&in MapAxis, string orientation)
{
	Vector Result = Vector(0,0,0);
	if (orientation.opEquals("floor")) Result = Vector((PlayerAxis.x+MapAxis.x), (PlayerAxis.y+MapAxis.y), MapAxis.z);
	if (orientation.opEquals("north")) Result = Vector((PlayerAxis.x+MapAxis.x), MapAxis.y, (PlayerAxis.y+MapAxis.z));
	if (orientation.opEquals("east")) Result = Vector(MapAxis.x,((PlayerAxis.x/-1)+MapAxis.y), (PlayerAxis.y+MapAxis.z));
	if (orientation.opEquals("south")) Result = Vector(((PlayerAxis.x/-1)+MapAxis.x), MapAxis.y, (PlayerAxis.y+MapAxis.z));
	if (orientation.opEquals("west")) Result = Vector(MapAxis.x,(PlayerAxis.x+MapAxis.y), (PlayerAxis.y+MapAxis.z));

	// technically, you can start adding your own orientations here.
	if (orientation.opEquals("3D_Ceiling")) Result = Vector( (PlayerAxis.x+MapAxis.x), (PlayerAxis.y+MapAxis.y), ((PlayerAxis.z/-1)+MapAxis.z));


	if (lotsofdevmsg) g_Game.AlertMessage(at_console, "Origin changed: X %2 - Y %3 - Z %3\n", Result.x, Result.y, Result.z);
	return Result;
}

CBaseEntity@ CreateBrushCopy( const string& in szTemplateTargetname, const string& in szTargetname )
{
	CBaseEntity@ pEntity = g_EntityFuncs.FindEntityByTargetname( null, szTemplateTargetname );

	if (devmsg) g_Game.AlertMessage(at_console, "Creating new entity %1 from %2. Model used %3\n", szTargetname,szTemplateTargetname,pEntity.pev.model);

	if( pEntity is null )
	{
		//Brush was removed.
		return null;
	}

	//Sanity check. Brush models start with a *
	if( !string( pEntity.pev.model ).StartsWith( "*" ) )
	{
		return null;
	}

	Vector vecOrigin = pEntity.pev.origin;	//Set destination origin here
	CBaseEntity@ pNew = g_EntityFuncs.Create( "func_wall", vecOrigin, g_vecZero, true );

	if( pNew is null )
	{
		//Couldn't create the entity for some reason
		return null;
	}

	//Copy the brush model.
	g_EntityFuncs.SetModel(pNew,pEntity.pev.model);


	//Give it a name
	pNew.pev.targetname = szTargetname;

	//Copy additional keyvalues here
	g_EntityFuncs.DispatchKeyValue(pNew.edict(),"renderamt", pEntity.pev.renderamt);
	g_EntityFuncs.DispatchKeyValue(pNew.edict(),"renderfx", pEntity.pev.renderfx);
	g_EntityFuncs.DispatchKeyValue(pNew.edict(),"rendermode", pEntity.pev.rendermode);

	g_EntityFuncs.DispatchSpawn( pNew.edict() );

	return pNew;
}

HookReturnCode ClientPutInServer( CBasePlayer@ pPlayer )
{
	for (uint i = 0; i < MiniMaps.length; ++i)
	{
		string NewTargetname = MiniMaps[i][5] + pPlayer.entindex();
		if (devmsg) g_Game.AlertMessage(at_console, "New Brush: %1\n", NewTargetname);

		CreateBrushCopy(MiniMaps[i][6],NewTargetname);
	}
	return HOOK_CONTINUE;
}

HookReturnCode ClientDisconnect( CBasePlayer@ pPlayer )
{
	for (uint i = 0; i < MiniMaps.length; ++i)
	{
		string Targetname = MiniMaps[i][5] + pPlayer.entindex();

		CBaseEntity@ DeleteMe = null;
		@DeleteMe = g_EntityFuncs.FindEntityByTargetname(null, Targetname);

		if (DeleteMe !is null)
		{
			if (devmsg) g_Game.AlertMessage(at_console, "Deleted Brush: %1\n", Targetname);
			g_EntityFuncs.Remove(DeleteMe);
		}
	}
	return HOOK_CONTINUE;
}


