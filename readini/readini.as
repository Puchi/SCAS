// read  files in .ini format. terrible performance!
// Author: Puchi
// With great help by: GeckonCZ, H2Whoa, Zode, Sniper
// Include this and use: MyString = readini("file","section","item");

string Readini(string &in filename, string &in section, string &in item)
{
	string cLine;

	bool SectionFound	= false;
	bool ItemFound		= false;

	File@ file = g_FileSystem.OpenFile(filename, OpenFile::READ);

	if (file !is null && file.IsOpen())
	{
		while(!file.EOFReached())
		{
			file.ReadLine(cLine);
			//g_Game.AlertMessage(at_console,"-------------------------\nSubstring: %1\nStartsWith: %2\nEndsWith: %3\nLength: %4\n",cLine.SubString(1, (cLine.Length()-2)),cLine.StartsWith("["),cLine.EndsWith("]"),cLine.Length());

			if (SectionFound && cLine.SubString(0,item.Length()) == item)
			{
				//g_Game.AlertMessage(at_console,"-------------------------\nFound the item aswell!\n");
				//g_Game.AlertMessage(at_console,"Content: %1\ncleaned content: %2\n-------------------------\n",cLine, cLine.SubString(item.Length()+1,cLine.Length()));
				ItemFound = true;
				file.Close();
				return cLine.SubString(item.Length()+1,cLine.Length());
			}
			else if (SectionFound && ((cLine.StartsWith("[") && cLine.EndsWith("]")) || cLine.IsEmpty() || file.EOFReached()))
			{
				g_Game.AlertMessage(at_console,"-------------------------\nReadIni: Item not found!\nReadIni: At the next Section or end of File!\n-------------------------\n");
				file.Close();
				return "";
			}


			if ((cLine.SubString(1, cLine.Length()-2) == section) && cLine.StartsWith("[") && cLine.EndsWith("]") && !SectionFound)
			{
				//g_Game.AlertMessage(at_console,"Found the section!\n");
				SectionFound = true;
			}
		} // end while

		if(file.EOFReached() && (!SectionFound || !ItemFound))
		{
			g_Game.AlertMessage(at_console,"-------------------------\nReadIni: No Section or Item found.\nReadIni: Please check your script or file!\n-------------------------\n");
			file.Close();
			return "";
		}

	} // end if file null

	g_Game.AlertMessage(at_console,"-------------------------\nReadIni: Something went horribly wrong.\nReadIni: Most possible cause: File did not open.\nReadIni: Check the filename and if it exists!\n-------------------------\n");
	return "";

}//end void

void TestReadIni()
{
	g_Game.AlertMessage(at_console,"ReadIni: Result: %1\n",Readini("scripts/maps/store/test.txt","Origin","4"));
}
